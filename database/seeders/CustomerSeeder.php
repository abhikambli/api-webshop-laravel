<?php

namespace Database\Seeders;

use App\Models\Customer;
use File;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Log;
use Validator;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imported = 0;
        $failed = 0;

        try {
            Customer::truncate();
            $csvData = fopen(base_path('database/csv/customers.csv'), 'r');
            $csvLogsPath = storage_path('logs/csv/');
            $processedCsvPath = $csvLogsPath . 'customer-processed-' . uniqid() . '.csv';
            File::ensureDirectoryExists($csvLogsPath);
            $processedCsv = fopen($processedCsvPath, "w");

            $headerRow = true;
            while (($data = fgetcsv($csvData)) !== false) {
                $processedData = $data;
                if (!$headerRow) {
                    $registeredSinceArr = explode(',', $data['4']);
                    unset($registeredSinceArr[0]);
                    $registeredSince = \DateTime::createFromFormat('F j,Y', implode(',', $registeredSinceArr));

                    $customer = [
                        'job_title' => $data['1'],
                        'email' => $data['2'],
                        'full_name' => $data['3'],
                        'registered_since' => $registeredSince->format('Y-m-d'),
                        'phone' => $data['5'],
                    ];
                    $validator = Validator::make($customer, [
                        'job_title' => 'required|max:255',
                        'email' => 'required|email',
                        'full_name' => 'required|max:255',
                        'registered_since' => 'required',
                        'phone' => 'required',
                    ]);
                    if (!$validator->fails()) {
                        Customer::create($customer);
                        $imported++;
                        $processedData[] = '';
                    } else {
                        $errors = $validator->errors()->all();
                        $failed++;
                        $processedData[] = implode(' | ', $errors);
                    }
                } else {
                    $processedData[] = 'Errors';
                }
                $headerRow = false;
                fputcsv($processedCsv, $processedData);
            }
            fclose($csvData);
            $result = "Imported: $imported customer records, Failed: $failed customer records";
            $result .= "\nRefer customer processed csv: " . $processedCsvPath;
            if ($failed == 0) {
                $this->command->info($result);
                Log::info($result);
            } else {
                $this->command->error($result);
                Log::error($result);
            }
        } catch (\Exception $e) {
            $result = "Imported: $imported customer records";
            $result .= "\nError: " . $e->getMessage();
            $this->command->error($result);
            Log::error($result);
        }
    }
}
