<?php

namespace Database\Seeders;

use App\Models\Product;
use File;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Log;
use Validator;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imported = 0;
        $failed = 0;
        
        try {
            Product::truncate();
            $csvData = fopen(base_path('database/csv/products.csv'), 'r');
            $csvLogsPath = storage_path('logs/csv/');
            $processedCsvPath = $csvLogsPath . 'products-processed-' . uniqid() . '.csv';
            File::ensureDirectoryExists($csvLogsPath);
            $processedCsv = fopen($processedCsvPath, "w");

            $headerRow = true;
            while (($data = fgetcsv($csvData)) !== false) {
                $processedData = $data;
                if (!$headerRow) {
                    $product = [
                        'name' => $data['1'],
                        'price' => $data['2'],
                    ];
                    $validator = Validator::make($product, [
                        'name' => 'required|unique:products|max:255',
                        'price' => 'required|numeric',
                    ]);
                    if (!$validator->fails()) {
                        Product::create($product);
                        $imported++;
                        $processedData[] = '';
                    } else {
                        $errors = $validator->errors()->all();
                        $failed++;
                        $processedData[] = implode(' | ', $errors);
                    }
                } else {
                    $processedData[] = 'Errors';
                }
                $headerRow = false;
                fputcsv($processedCsv, $processedData);
            }
            fclose($csvData);
            $result = "Imported: $imported product records, Failed: $failed product records";
            $result .= "\nRefer product processed csv: " . $processedCsvPath;
            if ($failed == 0) {
                $this->command->info($result);
                Log::info($result);
            } else {
                $this->command->error($result);
                Log::error($result);
            }
        } catch (\Exception $e) {
            $result = "Imported: $imported product records";
            $result .= "\nError: " . $e->getMessage();
            $this->command->error($result);
            Log::error($result);
        }
    }
}
