<?php

namespace App\Resolvers;

class PaymentPlatformResolver
{
    public function resolveService($name)
    {
        $service = config("services.{$name}.class");

        if ($service) {
            return resolve($service);
        }

        throw new \Exception('The selected platform is not in the configuration');
    }
}
