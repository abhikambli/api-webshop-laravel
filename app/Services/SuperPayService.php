<?php

namespace App\Services;

use App\Models\Order;
use Http;

// use App\Traits\ConsumesExternalServices;

class SuperPayService
{
    // use ConsumesExternalServices;

    protected $baseUri;

    public function __construct()
    {
        $this->baseUri = config('services.superpay.base_uri');
    }

    public function handlePayment(Order $order)
    {
        $payload = [
            "order_id" => $order->id,
            "customer_email" => $order->customer->email,
            "value" => $order->total,
        ];
        $response = Http::connectTimeout(300)->post($this->baseUri . '/pay', $payload);
        if ($response->successful() && $response->json('message') == 'Payment Successful') {
            return [
                'success' => true,
                'message' => $response->json('message')
            ];
        }
        return [
            'success' => false,
            'message' => $response->json('message')
        ];
    }
}
