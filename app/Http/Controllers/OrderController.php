<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Resolvers\PaymentPlatformResolver;
use Http;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Order::with('orderProducts')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'customer_id' => ['required', Rule::exists('customers', 'id')],
        ]);

        $order = Order::create([
            'customer_id' => $request->customer_id,
        ]);

        return [
            'message' => 'Order created succesfully',
            'order' => $order,
        ];
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $order = Order::where('id', $id)->first();
        if ($order === null) {
            return response()->json([
                'message' => 'Order not found',
            ], 404);
        }

        return [
            'order' => $order,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'customer_id' => ['required', Rule::exists('customers', 'id')],
        ]);
        $order = Order::where('id', $id)->first();
        if ($order === null) {
            return response()->json([
                'message' => 'Order not found',
            ], 404);
        }

        if ($request->get('customer_id')) {
            $order->customer_id = $request->get('customer_id');
        }
        if (!$order->save()) {
            return response()->json([
                'message' => 'Failed to update the order',
            ], 500);
        }

        return [
            'message' => 'Order updated succesfully',
            'order' => $order,
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $order = Order::where('id', $id)->first();
        if ($order === null) {
            return response()->json([
                'message' => 'Order not found',
            ], 404);
        }

        if (!$order->delete()) {
            return response()->json([
                'message' => 'Failed to delete the order',
            ], 500);
        }

        return [
            'message' => 'Order deleted succesfully',
        ];
    }

    public function addProducts(Request $request, int $id)
    {
        $order = Order::where('id', $id)->first();
        if ($order === null) {
            return response()->json([
                'message' => 'Order not found',
            ], 404);
        }

        if ($order->is_payed) {
            throw ValidationException::withMessages(['order_id' => 'Unable to add product since order is already paid']);
        }

        $request->validate([
            'product_id' => ['required', Rule::exists('products', 'id')],
        ]);

        $product = Product::where('id', $request->product_id)->first();

        $orderProduct = OrderProduct::create([
            'order_id' => $id,
            'product_id' => $request->product_id,
            'price' => $product->price
        ]);

        $order->increment('total', $product->price);

        return [
            'message' => 'Product added to order succesfully',
            'order_product' => $orderProduct
        ];
    }

    public function pay(Request $request, int $id)
    {
        $order = Order::where('id', $id)->with('customer')->first();
        if ($order === null) {
            return response()->json([
                'message' => 'Order not found',
            ], 404);
        }

        if ($order->is_payed) {
            throw ValidationException::withMessages(['order_id' => 'Payment already done for the order']);
        }

        // can be taken from request payload in future
        $paymentPlatform = 'superpay';

        $paymentPlatformResolver = new PaymentPlatformResolver();
        $paymentPlatform = $paymentPlatformResolver->resolveService($paymentPlatform);

        $response = $paymentPlatform->handlePayment($order);
        if ($response['success']) {
            $order->is_payed = 1;
            $order->save();
        }

        return $response;
    }
}
